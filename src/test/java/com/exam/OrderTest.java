package com.exam;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

public class OrderTest {

	Order order = null;
	
	@Before
	public void setUpDate(){
		
		Item item = null;
		OrderItem orderItem = null; 
		ArrayList<OrderItem> list = new ArrayList<OrderItem>();	
		
		item = new Item(new Integer(1), "FrechFries", (float)5.62);
		orderItem = new OrderItem(item, ItemTypes.MATERIAL);
		list.add(orderItem);

		item = new Item(new Integer(2), "Juice", (float)15.22);
		orderItem = new OrderItem(item, ItemTypes.MATERIAL);
		list.add(orderItem);
		
		item = new Item(new Integer(3), "Burger", (float)25.37);
		orderItem = new OrderItem(item, ItemTypes.MATERIAL);
		list.add(orderItem);
		
		item = new Item(new Integer(4), "Steak", (float) 65.82);
		orderItem = new OrderItem(item, ItemTypes.MATERIAL);
		list.add(orderItem);
		
		item = new Item(new Integer(5), "Chicken", (float)33.58);
		orderItem = new OrderItem(item, ItemTypes.MATERIAL);
		list.add(orderItem);
		
		item = new Item(new Integer(6), "Delivery", (float)28);
		orderItem = new OrderItem(item, ItemTypes.SERVICE);
		list.add(orderItem);
		
		item = new Item(new Integer(7), "Serving", (float)12.75);
		orderItem = new OrderItem(item, ItemTypes.SERVICE);
		list.add(orderItem);
		
		item = new Item(new Integer(8), "Cashier", (float)5);
		orderItem = new OrderItem(item, ItemTypes.SERVICE);
		list.add(orderItem);
		
		item = new Item(new Integer(9), "Chocolate Cake", (float)3.48);
		orderItem = new OrderItem(item, ItemTypes.MATERIAL);
		list.add(orderItem);
		
		item = new Item(new Integer(10), "Milk Shake", (float)1.78);
		orderItem = new OrderItem(item, ItemTypes.MATERIAL);
		list.add(orderItem);
		
		OrderItem[] orderItems = new OrderItem[list.size()];
		orderItems = list.toArray(orderItems);
		
		order = new Order(orderItems);
		
		
	}
	
	
	@Test
	public void testGetOrderTotal(){
		
		float orderTotal = order.getOrderTotal((float) .085);
		System.out.println("The order total is " + orderTotal);
	}

	@Test
	public void testOrderItemCollectionSortedByName(){
		Collection<OrderItem> list = order.getItems();
		System.out.println("The items in order are \n" + list);
	}

	@Test
	public void testGetOrderItemByKey(){
		OrderItem item = order.getOrderItemByKey(new Integer(1));
		System.out.println("The item details for the item id " + item.getItem().getKey().intValue() + " is "+ item);
	}

	@Test
	public void testGetOrderItemByName(){
		OrderItem item = order.getOrderItemByName("Serving");
		System.out.println("The item details for the item name " + item.getItem().getName() + " is "+ item);
	}

	@Test
	public void testGetOrderItemsByOrderTypeService(){
		ArrayList<OrderItem> items = order.getOrderItemsByOrderType(ItemTypes.SERVICE);
		System.out.println("The items for the type " + ItemTypes.SERVICE + "\n" + items);
	}

	@Test
	public void testGetOrderItemsByOrderTypeMaterial(){
		ArrayList<OrderItem> items = order.getOrderItemsByOrderType(ItemTypes.MATERIAL);
		System.out.println("The items for the type " + ItemTypes.MATERIAL + "\n" + items);
	}

}
