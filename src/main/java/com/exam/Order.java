package com.exam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.math3.util.Precision;

public class Order implements Serializable {
	
	private static final long serialVersionUID = 1L; 
	private OrderItem[] orderItems;
	private Hashtable<Item, OrderItem> itemTable = null;
	private List<OrderItem> collection = null;
	public Order(OrderItem[] orderItems) {
		this.orderItems = orderItems;
	}

	/**
	 * Returns the total order cost after the tax has been applied
	 * @param taxRate
	 * @return
	 */
	public float getOrderTotal(float taxRate) {
		float orderToal = 0;
		if(orderItems != null && orderItems.length > 0){
			int noOfOrders = orderItems.length;
			OrderItem orderItem = null;
			for(int i = 0; i < noOfOrders; i ++){
				orderItem = orderItems[i];
				if(orderItem != null && orderItem.getItem() != null){
					switch (orderItem.getItemType()){
						case SERVICE:
							orderToal += orderItem.getItem().getPrice() * (1+taxRate);
							break;
						default:
							orderToal += orderItem.getItem().getPrice();
					}
				}
			}
		}
		orderToal = Precision.round(orderToal, 2);
		return orderToal; 
	}

	/**
	 * Returns a Collection of all items sorted by item name (case-insensitive).
	 *
	 * @return Collection
	 */
	public Collection<OrderItem> getItems() {
		
		
		if(collection == null && orderItems != null && orderItems.length > 0){
			collection = new ArrayList<OrderItem>();
			
			int noOfOredrs = orderItems.length;
			for(int i = 0; i < noOfOredrs; i++){
				collection.add(orderItems[i]);
			}
			this.sortCollectionBasedOnName(collection);	
			
		}
		return collection; 
	}

	/**
	 * Returns the Hashtable
	 * @return
	 */
	public Hashtable<Item, OrderItem> getItemsAsMp() {
		
		
		if(itemTable == null && orderItems != null && orderItems.length > 0){
			itemTable = new Hashtable<Item, OrderItem>();
			
			int noOfOredrs = orderItems.length;
			for(int i = 0; i < noOfOredrs; i++){
				if(orderItems[i] != null && orderItems[i].getItem() != null){
					itemTable.put(orderItems[i].getItem(), orderItems[i]);
				}
			}
		}
		return itemTable; 
	}
	
	/**
	 * returns OrderItem based on item key
	 * @param key
	 * @return
	 */
	public OrderItem getOrderItemByKey(Integer key) {
		OrderItem orderItem = null;
		if(orderItems != null && orderItems.length > 0){
			int noOfOredrs = orderItems.length;
			for(int i = 0; i < noOfOredrs; i++){
				if(orderItems[i] != null && orderItems[i].getItem() != null){
					if(key != null && orderItems[i].getItem().getKey().equals(key)){
						orderItem = orderItems[i];
					}
				}
			}
		}
		return orderItem; 
	}

	/**
	 * returns order item based on item name
	 * @param name
	 * @return
	 */
	public OrderItem getOrderItemByName(String name) {
		OrderItem orderItem = null;
		if(orderItems != null && orderItems.length > 0){
			int noOfOredrs = orderItems.length;
			for(int i = 0; i < noOfOredrs; i++){
				if(orderItems[i] != null && orderItems[i].getItem() != null){
					if(name != null && orderItems[i].getItem().getName().equalsIgnoreCase(name)){
						orderItem = orderItems[i];
					}
				}
			}
		}
		return orderItem; 
	}

	/**
	 * returns list of order items in an order based on item type
	 * @param type
	 * @return
	 */
	public ArrayList<OrderItem> getOrderItemsByOrderType(ItemTypes type) {
		ArrayList<OrderItem> orderItemsList = null;
		if(orderItems != null && orderItems.length > 0){
			int noOfOredrs = orderItems.length;
			orderItemsList = new ArrayList<OrderItem>();
			for(int i = 0; i < noOfOredrs; i++){
				if(orderItems[i] != null){
					if(orderItems[i].getItemType() == type){
						orderItemsList.add(orderItems[i]);
					}
				}
			}
		}
		return orderItemsList; 
	}
	
	/**
	 * Sorting the array based on item name.
	 * @param collection
	 */
	private void sortCollectionBasedOnName(List<OrderItem> collection){
		
		//I know it may not be empty but still doing it,
		if(collection != null && !collection.isEmpty()){
			Collections.sort(collection, new Comparator<OrderItem>(){
	            public int compare(OrderItem orderItem1, OrderItem orderItem2) {
	                return orderItem1.getItem().getName().compareToIgnoreCase(orderItem2.getItem().getName());
	            }
			});
		}
	}
}
