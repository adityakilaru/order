package com.exam;

public class OrderItem {

	private ItemTypes itemType;
	private Item item;

	public OrderItem(Item item, ItemTypes itemType) {
		if (item != null) {
			this.item = item;
		} else {
			this.item = new Item(null, null, 0);
		}
		if (itemType != null) {
			this.itemType = itemType;
		} else {
			this.itemType = ItemTypes.MATERIAL;
		}
	}

	public ItemTypes getItemType() {
		return itemType;
	}

	public Item getItem() {
		return item;
	}

	@Override
	public String toString() {
		return "OrderItem [itemType=" + itemType + ", item=" + item + "] \n";
	}

}
