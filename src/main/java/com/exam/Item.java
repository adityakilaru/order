package com.exam;

public class Item {
	private Integer key;
	private String name;
	private float price;

	public Item(Integer key, String name, float price) {
		if(key != null){
			this.key = key;
		} else {
			this.key = new Integer(100);
		}
		if(name != null){
			this.name = name;
		} else {
			this.name = "GENERIC"; 
		}
		this.price = price;
	}

	public Integer getKey() {
		return key;
	}

	public String getName() {
		return name;

	}

	public float getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return "Item [key=" + key + ", name=" + name + ", price=" + price + "]";
	}
	
	
}
